<?php
  session_start();
  require('connection.php');

    if(!empty($_SESSION['user_id'])){
 				//redirect to user's Profile
				header('Location:Profile.php');        
    }

	if(isset($_POST['txtuname'])) {
      $target_dir = "Data/Profile Pictures/";
      $target_file = $target_dir . basename($_FILES["pic"]["name"]);
      $uploadOk = 1;
      $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

      $username = $_POST['txtuname'];
      $useremail = $_POST['txtemail']; 
      $password = $_POST['txtpass'];

      if($_FILES['pic']['type'] == "image/jpg" ||$_FILES['pic']['type'] == "image/png" ||$_FILES['pic']['type'] == "image/jpeg" 
      || $_FILES['pic']['type'] == "image/gif" ){
          if (move_uploaded_file($_FILES["pic"]["tmp_name"], $target_file)) {
            $pic = $_FILES['pic']['name'];
          } else {
                $msg = "Sorry, there was an error uploading your file.";
              }
      }

  if(!Signin($useremail, $password)){
        // get user id from signin select command
        $userid = Signup($username, $useremail, $password, $pic);
        if($userid != 0){
            //assign userid to session
            echo $_SESSION['user_id']=$userid;
            //redirect to user's Profile
            header('Location:Profile.php');
            exit();
        }
        else if($userid == 0){
          $msg = "error inserting";
        }
      }
  }
?>

<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Sign Up - ArticleYou</title>
    <script src="http://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>   
    <link rel="stylesheet" href="assets/css/reset.css">
    <link rel="stylesheet prefetch" href='http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="assets/css/style.css">
  </head>

  <body>

    <form class="login" method="post" action="signup.php" enctype="multipart/form-data">

        <legend class="legend">Sign Up</legend>

        <div class="input">
          <input type="text" name="txtuname" placeholder="Username" required />
          <span><i class="fa fa-user-o"></i></span>
        </div>

        <div class="input">
          <input type="email" name="txtemail" placeholder="Email" required />
          <span><i class="fa fa-envelope-o"></i></span>
        </div>

        <div class="input">
          <input type="password" name="txtpass" placeholder="Password" required />
          <span><i class="fa fa-lock"></i></span>
        </div>

        <div class="input">
          <input type="password" name="txtcpass" placeholder="Confirm Password" required />
          <span><i class="fa fa-lock"></i></span>
        </div>

        <div class="input">
          <input type="file" name="pic" placeholder="Picture" required />
          <span><i class="fa fa-picture-o"></i></span>
        </div>
        <button name="btnsignup" class="submit"><i class="fa fa-long-arrow-right"></i></button>
    </form>

    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="assets/js/index.js"></script>    
  </body>
</html>