<?php
 session_start();
//required connection file to connect to the database and access other DB functions
  require('connection.php');    

  if(!empty($_SESSION['user_id'])){ 
      //if already loggedin show all recent posts 
	     RetriveAllArticles();
  }
  else{
	  //show only public visible posts
	  RetrivePublicArticles();
  }

   $result = $_SESSION['$aresult'];
   $row = $_SESSION['$allrow'];
?>

<!DOCTYPE HTML>

<html>
<!--head-->
	<head>
		<title>Dashboard - ArticleYou</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body>
<!--body-->
	<body style="background: url(http://subtlepatterns.com/patterns/sativa.png) repeat fixed;">
	<!-- Wrapper -->
			<div id="wrapper">
				<!-- Header -->
					<header id="header">
						<h1><a href="#">ArticleYou</a></h1>
						<nav class="links">
							<ul>
								<li><a href="#" class="active">Dashboard</a></li>
							</ul>
						</nav>
						<nav class="main">
							<ul>
								<li class="search">
									<a class="fa-search" href="#search">Search</a>
									<form id="search" method="POST" action="#">
										<input type="text" name="query" placeholder="Search" />
									</form>
								</li>
								<li class="menu">
									<a class="fa-bars" href="#menu">Menu</a>
								</li>
							</ul>
						</nav>
					</header>

				<!-- Menu -->
					<section id="menu" style="background: url(http://subtlepatterns.com/patterns/sativa.png) repeat fixed;">
						<section> 
							<ul class="links" >
								<li>
									<a href="#"  style="text-align:center">
										<h2>Create Your Account</h2>
										<p>Sign in or Sign up here</p>
									</a>
								</li>
							</ul>
						</section>
						<section>
							<ul class="actions vertical">
								<li><a href="signin.php" class="button big fit">Sign In</a></li>
							</ul>	
							<ul class="actions vertical">
								<li><a href="signup.php" class="button big fit">Sign up</a></li>
							</ul>
						</section>
					</section>

				<!-- Main -->
					<div id="main">

					<!-- Post -->
				        <?php foreach($result as $row){ ?>
							<article class="post">
								<header>
									<div class="title">
										<h2><a href="single.php?id=<?php echo $row['article_id']; ?>"><?php echo $row['article_title']; ?></a></h2>
										<p><?php echo $row['article_tags']; ?></p>
									</div>
									<div class="meta">
										<date class="published" ><?php echo $row['article_date']; ?></date>
										<a href="#" class="author">
											<span class="name"><?php echo $row['user_name']; ?></span>
											<img src="Data/Profile Pictures/<?php echo $row['user_pic']; ?>" alt="" />
										</a>
									</div>
								</header>
								<a href="single.php?id=<?php echo $row['article_id']; ?>" class="image featured">
									<img style="height: 400px;width: 100%;" src="Data/Article Images/<?php echo $row['article_image']; ?>" alt="" />
								</a>
								<footer>
									<ul class="actions">
										<li><a href="single.php?id=<?php echo $row['article_id']; ?>" class="button big">
											Continue Reading
										</a></li>
									</ul>
								</footer>
							</article>
						<?php } ?>
					</div>

				<!-- Sidebar -->
					<section id="sidebar">

						<!-- Intro -->
							<section id="intro">
								<a href="#" class="image"><img src="Data/logo.png" alt="" /></a>
								<header style="text-align:center">
									<h2>Article You</h2>
									<p >One Place for good readers and info geeks</p>
								</header>
							</section>

						<!-- About -->
							<section class="blurb">
								<h2>About</h2>
								<p>This website is a very good place for info geeks and article publishers. you can read and like along with post diffrent articles after your regstraction for free .<br />
                                keep enjoying.</p>
							</section>
            
						<!-- Footer -->
							<section id="footer">
								<ul class="icons">
									<li><a href="https://twitter.com/MaidahR" class="fa-twitter"><span class="label">Twitter</span></a></li>
									<li><a href="https://www.facebook.com/maidah.rizvi" class="fa-facebook"><span class="label">Facebook</span></a></li>
									<li><a href="http://cezy.org/profile/syedamaidahrizvi/2869298565" class="fa-instagram"><span class="label">Instagram</span></a></li>
									<li><a href="https://www.linkedin.com/in/maidah-rizvi-239b6ab2" class="fa-linkedin"><span class="label">RSS</span></a></li>
									<li><a href="#" class="fa-envelope"><span class="label">Email</span></a></li>
								</ul>
								<p class="copyright">&copy; Made by Syeda Maidah Rizvi</a>.</p>
							</section>
                    </section>
			</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>