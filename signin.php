<?php
  session_start();
  require('connection.php'); //required connection file to connect to the data base and other DB functions

  if(!empty($_SESSION['user_id'])){ 
      ///if already logged in go to profile page
				header('Location:Profile.php');      
  }

  if(isset($_POST['txtemail'])){ //because button runs action only and is not included in $_POST
      $useremail = $_POST['txtemail']; 
      $password = $_POST['txtpass'];

      //if registered then get user id from signin select command
      $userid = Signin($useremail, $password);

      if($userid != 0){ //if registered      
          //assign userid to session
          echo $_SESSION['user_id']=$userid;
          //redirect to user's Profile
          header('Location:Profile.php');
          exit();
      }
      else if($userid == 0){ //if not
      }
	}
	
?>

<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Sign In - ArticleYou</title>
    <script src="http://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>   
    <link rel="stylesheet" href="assets/css/reset.css">
    <link rel="stylesheet prefetch" href='http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="assets/css/style.css">
  </head>

  <body>

    <form class="login" method="POST" action="signin.php">
      <!--<fieldset>-->
        <legend class="legend">Sign In</legend>

        <div class="input">
          <input type="email" name="txtemail" placeholder="Email" required />
          <span><i class="fa fa-envelope-o"></i></span>
        </div>

        <div class="input">
          <input type="password" name="txtpass" placeholder="Password" required />
          <span><i class="fa fa-lock"></i></span>
        </div>

        <button type="submit" name="btnsignin" class="submit"><i class="fa fa-long-arrow-right"></i></button>
    </form>

    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="assets/js/index.js"></script>    
  </body>
</html>
