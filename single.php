<?php
 session_start();
//required connection file to connect to the data base and other DB functions
  require('connection.php');    
  
  	$id = $_GET['id'];
	$row = RetriveArticle($id);
	$title = $row['article_title'];
	$content = $row['article_content'];	
	$tags = $row['article_tags'];
	$author = $row['user_name'];
	$author_pic = $row['user_pic'];
	$image = $row['article_image'];
	$date = $row['article_date'];
?>


<!DOCTYPE HTML>

<html>
	<head>
		<title>Article - ArticleYou</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="single" style="background: url(http://subtlepatterns.com/patterns/sativa.png) repeat fixed;">

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Header -->
					<header id="header">
						<h1><a href="#">ArticleYou</a></h1>
						<nav class="links">
							<ul>
								<li><a href="index.php" class="active">Dashboard</a></li>
							</ul>
						</nav>
						<nav class="main">
							<ul>
								<li class="search">
									<a class="fa-search" href="#search">Search</a>
									<form id="search" method="get" action="#">
										<input type="text" name="query" placeholder="Search" />
									</form>
								</li>
								<li class="menu">
									<a class="fa-bars" href="#menu">Menu</a>
								</li>
							</ul>
						</nav>
					</header>

				<!-- Menu -->	
					<section id="menu" style="background: url(http://subtlepatterns.com/patterns/sativa.png) repeat fixed;">
						<section> 
							<ul class="links" >
								<li>
									<a href="#"  style="text-align:center">
										<h2>Create Your Account</h2>
										<p>Sign in or Sign up here</p>
									</a>
								</li>
							</ul>
						</section>
						<section>
							<ul class="actions vertical">
								<li><a href="signin.php" class="button big fit">Sign In</a></li>
							</ul>	
							<ul class="actions vertical">
								<li><a href="signup.php" class="button big fit">Sign up</a></li>
							</ul>
						</section>
					</section>

				<!-- Main -->
					<div id="main">

						<!-- Post -->
							<article class="post">
								<header>
									<div class="title">
										<h2><a href="#"><?php echo $title;?></a></h2>
										<p><?php echo $tags;?></p>
									</div>
									<div class="meta">
										<time class="published"><?php echo $date;?></time>
										<a href="#" class="author"><span class="name"><?php echo $author; ?></span>
										<img src="Data/Profile Pictures/<?php echo $author_pic; ?>" alt="" /></a>
									</div>
								</header>
								<span class="image featured"><img src="Data/Article Images/<?php echo $image;?>" alt="" /></span>
								<p><?php echo $content;?></p>
							</article>

					</div>

				<!-- Footer -->
					<section id="footer">
						<ul class="icons">
							<li><a href="https://twitter.com/MaidahR" class="fa-twitter"><span class="label">Twitter</span></a></li>
							<li><a href="https://www.facebook.com/maidah.rizvi" class="fa-facebook"><span class="label">Facebook</span></a></li>
							<li><a href="http://cezy.org/profile/syedamaidahrizvi/2869298565" class="fa-instagram"><span class="label">Instagram</span></a></li>
							<li><a href="https://www.linkedin.com/in/maidah-rizvi-239b6ab2" class="fa-linkedin"><span class="label">RSS</span></a></li>
							<li><a href="#" class="fa-envelope"><span class="label">Email</span></a></li>
						</ul>
						<p class="copyright">&copy; Made by Syeda Maidah Rizvi</a>.</p>
					</section>

			</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>
	</body>
</html>