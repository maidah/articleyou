<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $database = "articleyou";
    $GLOBALS['$date'] = date("Y-m-d");

    $GLOBALS['$conn'] = new mysqli($servername, $username, $password, $database);

            if ($GLOBALS['$conn']->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            else{ 
                return true;
            }

    //retrive only public articles
    function RetrivePublicArticles() {
        $sql = "SELECT a.`article_id`, a.`article_title`, a.`article_content`, a.`article_tags`, a.`article_image`, a.`article_date`, u.`user_name`, u.`user_pic` 
                FROM `article` a, user u 
                WHERE  `public_visibility` = 1 AND u.`user_id` = a.`user_id`
                ORDER BY `article_date` DESC;";

        $result = $GLOBALS['$conn']->query($sql);
        if ($result->num_rows > 0) {
                $_SESSION['$totalresult'] = $result->num_rows;
                    while($row = $result->fetch_assoc()) {
                        $_SESSION['$aresult'] = $result;
                        $_SESSION['$allrow'] = $row;
                                //return rows if succesfulls
                        return true;
                    }
        } else {
            return false;
        }
    }

    //retrive all articles (for reggistered users)
    function RetriveAllArticles(){
        $sql = "SELECT a.`article_id`, a.`article_title`, a.`article_content`, a.`article_tags`, a.`article_image`, a.`article_date`, u.`user_name`, u.`user_pic` 
                FROM `article` a, user u 
                WHERE u.`user_id` = a.`user_id`
                ORDER BY `article_date` DESC;";

        $result = $GLOBALS['$conn']->query($sql);
          if ($result->num_rows > 0) {
                $_SESSION['$totalresult'] = $result->num_rows;
                    while($row = $result->fetch_assoc()) {
                            $_SESSION['$aresult'] = $result;
                            $_SESSION['$allrow'] = $row;
                                    //return rows if succesfulls
                            return true;
                    }
        } else {
            return false;
        }
    }

    //retrive single article by id
    function RetriveArticle($id) {
        $sql = "SELECT a.`article_id`, a.`article_title`, a.`article_content`, a.`article_tags`, a.`article_image`, a.`article_date`, u.`user_name`, u.`user_pic` 
                FROM `article` a, user u 
                WHERE a.`article_id` = $id AND u.`user_id` = a.`user_id`";

        $result = $GLOBALS['$conn']->query($sql);
       if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                //return rows if succesfulls
                 return $row;
            }
        } else {
            return false;
        }
    }

    // verify email and password to login
    function Signin($email, $pass) {
        $sql = "SELECT `user_id`, `user_email`, `user_password` FROM `user` WHERE `user_email` = '$email' AND `user_password` = '$pass'";
        $result = $GLOBALS['$conn']->query($sql);
       if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                //return true if registered
                 return $row['user_id'];
            }
        } else {
            return 0;
        }
    }

    // verify email and password to login
    function Signup($uname, $email, $pass, $pic) {
        $sql = "INSERT INTO `user`(`user_name`, `user_password`, `user_email`, `user_pic`) VALUES ('$uname', '$pass', '$email', '$pic');";
        if ($GLOBALS['$conn']->query($sql) === TRUE) {
            $id = Signin($email,$pass);
            return $id;
        } else {
            echo "Error: " . $sql . "<br>" . $GLOBALS['$conn']->error;
            return 0;
        }
    }

    // retrive all data of user by User_id for PROFILE
    function RetriveUserDetails($userid) {
        $sql = "SELECT `user_name`, `user_email`, `user_pic` FROM `user` WHERE `user_id` = $userid;";
        $result = $GLOBALS['$conn']->query($sql);
       if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                //store data in session
                $_SESSION['user_name'] = $row['user_name'];
                $_SESSION['user_email'] = $row['user_email'];
                $_SESSION['user_pic'] = $row['user_pic'];
                if(RetriveUsersArticles($userid)){
                    return 1;
                }else{
                    return 2;
                }
            }
        } else {
            return 0;
        }
    }
    
    //insert new article details
    function NewArticle($title, $content, $tags, $image, $visibility, $userid) {    
        $article_date = $GLOBALS['$date'];
      $sql = "INSERT INTO `article`(`article_title`, `article_content`, 
        `article_tags`, `article_image`, `public_visibility`, `user_id`, `article_date`) 
         VALUES 
        ('$title', '$content', '$tags', '$image', $visibility, $userid, '$article_date');";
      if($GLOBALS['$conn']->query($sql) === TRUE){
            return true;
      }else {
            echo "Error: " . $sql . "<br>" . $GLOBALS['$conn']->error;
            return false;
        }
    }

    //retrive all user's posts
    function RetriveUsersArticles($userid) {
        $sql = "SELECT `article_id`,`article_title`, `article_content`, `article_tags`, `article_image`, `article_date` 
            FROM `article` 
            WHERE `user_id` = $userid
            ORDER BY `article_date` DESC";
            
        $result = $GLOBALS['$conn']->query($sql);
       if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
           $_SESSION['$uaresult'] = $result;
           $_SESSION['$uarow'] = $row;
                // foreach($result as $row){
                //     print_r($row['article_title']);
                //     print_r($row['article_content']);
                //     print_r($row['article_tags']);
                //     print_r($row['article_image']);
                //     print_r($row['article_date']);                    
                // }
                //return rows if succesfulls
                 return true;
            }
        } else {
            return false;
        }
    }

    function CloseConnection(){    
        $GLOBALS['$conn']->close();
    }

?>