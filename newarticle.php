<?php
  session_start();
  require('connection.php'); //required connection file to connect to the data base and other DB functions

  $name = $_SESSION['user_name'];
  $msg = " ";
  if(empty($_SESSION['user_id'])){ 
      //if already logged in go to profile page
		header('Location:signin.php');      
  }else{
      if(isset($_POST['btn_createarticle'])){
          $target_dir = "Data/Article Images/";
          $target_file = $target_dir . basename($_FILES["article_image"]["name"]);
          $uploadOk = 1;
          $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
     
          $title = $_POST['txttitle'];
          $content = $_POST['txtcontent'];
          $tags = $_POST['txttags'];
          $PV = $_POST['txtPV'];
        //saving article image
            if($_FILES['article_image']['type'] == "image/jpg" ||$_FILES['article_image']['type'] == "image/png" 
            || $_FILES['article_image']['type'] == "image/jpeg" || $_FILES['article_image']['type'] == "image/gif" ){
                if (move_uploaded_file($_FILES["article_image"]["tmp_name"], $target_file)) {
                    $image = $_FILES['article_image']['name'];
                } else {
                       $msg = "Sorry, there was an error uploading your file.";
                    }
            }

            if(NewArticle($title, $content, $tags, $image, $PV, $_SESSION['user_id'])){
                    $msg = "Your Article has been saved succesfully";
            }else{
                    $msg = "Error while saving your Article";
            }
      }
  }
?>
<!DOCTYPE HTML>

<html>
    <head>
        <title>New Article - ArticleYou</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
        <link href="assets/css/style.css" type="text/css" rel="stylesheet" />
        <link href="assets/css/main.css" type="text/css" rel="stylesheet" />
    </head>
    <body style="background: url(http://subtlepatterns.com/patterns/sativa.png) repeat fixed;">

     <!-- Header -->
        <header id="header">
            <h1><a href="Profile.php"><?php echo $name?></a></h1>
            <nav class="links">
                <ul>
                    <li><a href="index.php" class="active">Dashboard</a></li>
                </ul>
            </nav>
            <nav class="main">
                <ul>
                    <li><a href="logout.php" class="fa-sign-out">Logout</a></li>                    
                </ul>
            </nav>
        </header>

         <!-- new article -->    
        <div class="container">
            <div class="bdrbx">
                <div class="col-lg-1 col-md-1 col-sm-2">
                </div>
                
                <div class="col-lg-10 col-md-10 col-sm-8">

                    <!--text boxes for Article creation-->
                    <form method="post" action="newarticle.php" enctype="multipart/form-data">

                            <a href="#" class="legend">New Article</a>
<br />
                            <input type="text" name="txttitle" placeholder="Article Title" required />
<br />
                            <textarea id="content" name="txtcontent" placeholder="Your Article Content" required ></textarea>
<br />
                            <input type="text" name="txttags" placeholder="Related Tags For your Article" required />
<br />  
                            <input type="file" name="article_image" placeholder="Image" required />
<br />
                            <input type="text" name="txtPV" placeholder="Make visible to public? 1 for yes, 0 for no." required />
                            
<!--                            <input type="radio" name="public_visibility" value="PV"  />Make visible to public?</br>
                            <input type="radio" name="public_visibility" value="NPV" />Don't make visible to public?</br>-->
<br />
                            <input type="submit" name="btn_createarticle" class="button big fit" value="Publish" required />

                         </form>
                         <div>
                            <p><?php echo $msg; ?></p>
                         </div>
                </div>

                <div class="col-lg-1 col-md-1 col-sm-2">
                </div>

            </div>
        </div>
<br />
        <div id="wrapper">
            <!-- Footer -->
                <section id="footer">
                    <ul class="icons">
                        <li><a href="https://twitter.com/MaidahR" class="fa-twitter"><span class="label">Twitter</span></a></li>
                        <li><a href="https://www.facebook.com/maidah.rizvi" class="fa-facebook"><span class="label">Facebook</span></a></li>
                        <li><a href="http://cezy.org/profile/syedamaidahrizvi/2869298565" class="fa-instagram"><span class="label">Instagram</span></a></li>
                        <li><a href="https://www.linkedin.com/in/maidah-rizvi-239b6ab2" class="fa-linkedin"><span class="label">RSS</span></a></li>
                        <li><a href="#" class="fa-envelope"><span class="label">Email</span></a></li>
                    </ul>
                    <p class="copyright">&copy; Made by Syeda Maidah Rizvi</a>.</p>
                </section>
         </div>

         
		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>
    </body>
</html>